package ru.t1.semikolenov.tm.listener.data;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.semikolenov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.semikolenov.tm.listener.AbstractListener;

@Getter
@Component
public abstract class AbstractDataListener extends AbstractListener {

    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;

}